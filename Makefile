ex:
	gcc -lm ex.c -o ex
ff:
	./ex > ex.ff
png:
	./ex | ff2png > ex.png
anim:
	gcc -lm exanim.c -o exanim
	./exanim
logo:
	gcc -lm logo.c -o logo
	./logo | ff2png > logo.png
	rm logo
both: ex ff png
.PHONY: ex both anim logo
