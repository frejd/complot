/* ex.c: plots exp(1 / z) on the unit square to stdout */
#include <complex.h>
#include <stdio.h>

#include "complot.h"

complex double
cexpinv(complex double z)
{
	return cexp(1 / z);
}

int
main()
{
	complot_unisq(cexpinv, 256, CONTOUR, stdout);
}
