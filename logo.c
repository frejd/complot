#include <complex.h>
#include <stdio.h>

#include "complot.h"

complex double
logofunc(complex double z)
{
	return 2 / (1 - z);
}

int
main()
{
	complot_func(logofunc, 256, CONTOUR, stdout);
}
