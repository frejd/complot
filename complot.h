/*
 * Copyright (c) 2021 Frej Dahlin <frej.weistrom_dahlin@math.lu.se>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *
 * complot is a single header library for plotting complex number using
 * colorings. 
 *
 * The provided functions are: complot, complot_func, and, complot_anim.
 * The first of these take in complex values, dimensions for the output image,
 * a coloring, and a stream to send the output. The other two are convenience
 * functions to quickly be able to plot functions on the complex unit square.
 * Noting that complot_anim expects a function taking in a complex number
 * and an unsigned int = frame number.
 *
 * A coloring is a function from the complex plane into the RGB-color space.
 * The included colorings are defined as macros, e.g. "BANDED", which is the
 * default.
 */
#ifndef COMPLOTLIB_H
#define COMPLOTLIB_H

#include <arpa/inet.h>

#include <complex.h>
#include <errno.h>
#include <math.h> 
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARGTOHUE(A)		(RADTODEG(A) + ((RADTODEG(A) > 0.0) ? 0.0 : 360.0))
#define FRACTIONALPART(X)	((X) - floor(X))
#define HSLTORGB(H, S, L, RGB)	do {					\
	(RGB)[0] = HSLTORGB_F(H, S, L, 0);				\
	(RGB)[1] = HSLTORGB_F(H, S, L, 8);				\
	(RGB)[2] = HSLTORGB_F(H, S, L, 4);				\
} while (0)
#define HSLTORGB_F(H, S, L, N)	(255 * ((L) - (S) * MIN((L), 1 - (L)) * \
MAX(-1, MIN3(-3 + fmod((N) + (H) / 30, 12), 9 - fmod((N) + (H) / 30, 12), 1))))
#define HUETORGB(H, RGB)	HSLTORGB(H, 1.0, 0.5, RGB)	
#define LEN(X) 			(sizeof(X) / sizeof(*(X)))
#define MAX(A, B) 		(((A) >= (B)) ? (A) : (B))
#define MIN(A, B) 		(((A) <= (B)) ? (A) : (B))
#define MIN3(A, B, C)		MIN(MIN(A, B), C)
#define RADTODEG(R)		((R) * 180.0 * M_1_PI)

#define BANDED			complot_coloring_banded
#define CONTOUR			complot_coloring_contour
#define BINARY			complot_coloring_binary
#define RIEMANN			complot_coloring_riemann
#define SMOOTH			complot_coloring_smooth

void
complot_coloring_banded(complex double z, uint16_t rgb[3])
{
	double hue;

	hue = ARGTOHUE(carg(z));
	hue -= fmod(hue, 30);
	HUETORGB(hue, rgb);
}

void
complot_coloring_binary(complex double z, uint16_t rgb[3])
{
	rgb[0] = rgb[1] = rgb[2] = cimag(z) > 0 ? 0 : UINT16_MAX;
}

void
complot_coloring_contour(complex double z, uint16_t rgb[3])
{
	double x;

	if (((x = cabs(z)) < 1) ? x : FRACTIONALPART(log(x)) < 0.8)
		complot_coloring_banded(z, rgb);
	else
		rgb[0] = rgb[1] = rgb[2] = 0;
}

void
complot_coloring_riemann(complex double z, uint16_t rgb[3])
{
	double x;

	x = sqrt(cabs(z));
	HSLTORGB(ARGTOHUE(carg(z)), 1, x / (1 + x), rgb);
}

void
complot_coloring_smooth(complex double z, uint16_t rgb[3])
{
	HUETORGB(ARGTOHUE(carg(z)), rgb);			
}

/* complot: plots complex values to stream in farbfeld format. */
int
complot(double complex *vals, uint32_t w, uint32_t h,
    void (*coloring)(double complex, uint16_t [3]), FILE *stream)
{
	uint32_t hdr[4], i;
	uint16_t rgba[4];
	
	if (coloring == NULL)
		coloring = CONTOUR;
	if (stream == NULL)
		stream = stdout;
	memcpy(hdr, "farbfeld", sizeof("farbfeld") - 1);
	hdr[2] = htonl(w);
	hdr[3] = htonl(h);
	if (fwrite(hdr, sizeof(*hdr), LEN(hdr), stream) != LEN(hdr))
		goto writerr;

	rgba[3] = UINT16_MAX; /* only opaque images supported */
	for (i = 0; i < w * h; i++) {
		coloring(*vals++, rgba);
		if (fwrite(rgba, sizeof(*rgba), LEN(rgba), stream) != LEN(rgba))
			goto writerr;
	}
	
	if (fclose(stream)) {
		fprintf(stderr, "%s: fclose: complot\n", strerror(errno));
		return 1;
	}
	return 0;
writerr:
	fprintf(stderr, "%s: fwrite: complot\n", strerror(errno));
	return 1;
}

int
complot_anim(complex double (*anim)(complex double, unsigned int),
    unsigned int w, void (*coloring)(complex double, uint16_t [3]),
    unsigned int nframes, char *animname)
{
	FILE *frame;
	complex double *p, *vals;
	double x, y;
	unsigned int i;
	char *filename;
	
	if ((filename = malloc(strlen(animname) + strlen("00000.ff"))) == NULL
	    || (vals = calloc(w * w, sizeof(complex double))) == NULL) {
		fprintf(stderr, "%s: complot_anim\n", strerror(errno));
		return 1;
	}
	for (i = 0; i < nframes; i++) {
		p = vals;
		for (y = 1; y >= -1; y -= 2 / ((double)w - 1))
			for (x = -1; x <= 1; x += 2 / ((double)w - 1))
				*p++ = anim(x + I * y, i);	
		sprintf(filename, "%s%05d.ff", animname, i);
		frame = fopen(filename, "w");
		if (complot(vals, w, w, coloring, frame))
			return 1;
	}
	free(filename);
	free(vals);
	return 0;
}

int
complot_func(complex double (*func)(complex double), unsigned int w,
    void (*coloring)(complex double, uint16_t [3]), FILE *stream)
{
	complex double *p, *vals;
	double x, y;

	if ((vals = malloc(sizeof(complex double) * w * w)) == NULL) {
		fprintf(stderr, "%s: complot_func\n", strerror(errno));
		return 1;
	}
	p = vals;
	for (y = 1; y >= -1; y -= 2 / ((double)w - 1))
		for (x = -1; x <= 1; x += 2 / ((double)w - 1))
			*p++ = func(x + I * y); 

	if (complot(vals, w, w, coloring, stream))
		return 1;
	free(vals);
	return 0;
}
#endif
