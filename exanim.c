/* example.c: plots exp(1 / z) on the unit square to stdout */
#include <complex.h>
#include <stdio.h>
#include <math.h>

#include "complot.h"

#define DIM 128
#define MAXFRAME 256

complex double
cpowz(complex double z, unsigned int frame)
{
	double arg = 2 * M_PI * (double)frame / MAXFRAME;

	return cpow(z, cos(arg) + I * sin(arg));
}

int
main()
{
	complot_anim_unisq(cpowz, DIM, CONTOUR, MAXFRAME, "example");
}